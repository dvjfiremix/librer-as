package Librarys.UI;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;

import org.jetbrains.annotations.Nullable;

public class ColorGradientDrawable extends GradientDrawable {
    private final Paint mPaint;
    private int mRadius = 0;
    private int Colors;
    private float Corner;
    private boolean Circle = false;
    private boolean Rectangle = false;
    private float Opacidad = 1500;

    public ColorGradientDrawable(int color) {
        this.mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.Colors = color;
        this.mPaint.setColor(color);
        Circle = true;
    }

    public ColorGradientDrawable(@Nullable int color, @Nullable float corner, @Nullable float Opacidad) {
        this.mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.Colors = color;
        this.mPaint.setColor(color);
        if (Build.VERSION.SDK_INT >= 21) {
            this.Corner = corner;
        } else {
            if (corner >= 50) {
                this.Corner = corner - 30;
            } else {
                this.Corner = corner;
            }
        }
        Rectangle = true;
        if (Opacidad != 0) {
            this.Opacidad = Opacidad;
        }
    }

    @Override
    public void draw(final Canvas canvas) {
        if (Circle) {
            final Rect bounds = getBounds();
            mPaint.setShader(new LinearGradient(0, 0, 0, 1500, Colors, Color.BLACK, Shader.TileMode.MIRROR));
            canvas.drawCircle(bounds.centerX(), bounds.centerY(), mRadius, mPaint);
        } else if (Rectangle) {
            final RectF bounds = new RectF(0, 0, getBounds().width(), getBounds().height());
            mPaint.setShader(new LinearGradient(0, 0, 0, Opacidad, Colors, Color.BLACK, Shader.TileMode.MIRROR));
            canvas.drawRoundRect(bounds, Corner, Corner, mPaint);
        }
    }

    @Override
    protected void onBoundsChange(final Rect bounds) {
        super.onBoundsChange(bounds);
        mRadius = Math.min(bounds.width(), bounds.height()) / 2;
    }

    @Override
    public void setAlpha(final int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(final ColorFilter cf) {
        mPaint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}