//Cómo usar la librería de gradiente

1.- Creas un objeto que extienda de ColorGradientDrawable
2.- El color debe ser en número para eso usa un Color.parseColor("colorHex");
quedando de la siguiente manera.
    Ejemplo: usando un FrameLayout con un Ovalo gradiente
        int color = Color.parseColor("#FF5252");

        idFrameLayout.setBackground(new ColorGradientDrawable(color));
    Ejemplo: usando un FrameLayout con un Rectangulo gradiente
        int color = Color.parseColor("#FF5252");
        float corner = 50;
        float opacidad = 1500;

        idFrameLayout.setBackground(new ColorGradientDrawable(color,corner,opacidad));

//Cómo usar la librería de TriangleShape dentro del XML
    <TriangleShape
                android:id="@+id/triangulo"
                android:layout_width="80dp"
                android:layout_height="60dp"
                android:layout_alignParentEnd="true" />

Nota: esta librería no tiene ajustes mediante xml, se hace directo por código
 int color = Color.parseColor("#FF5252");
 triangulo.setColors(color);
        
